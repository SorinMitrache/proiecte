/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatelefonica;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @ Clasa abstracta
 */
class NrTel implements Serializable{

    private String nrTel;
    private Boolean isMobil;

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NrTel other = (NrTel) obj;
        return Objects.equals(this.nrTel, other.nrTel);
    }

    public NrTel(String nrTel) {
        this.nrTel = nrTel;
        this.isMobil = nrTel.startsWith("07");
    }

    public String getNrTel() {
        return nrTel;
    }

    public Boolean getIsMobil() {
        return isMobil;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    public void setIsMobil(Boolean isMobil) {
        this.isMobil = isMobil;
    }

    @Override
    public String toString() {
        return this.nrTel + " \t" + (isMobil?"Mobil":"Fix") ;
    }

}
