/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatelefonica;

import java.io.*;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractListModel;

/**
 *
 * @author sorin
 */
public class CarteDeTelefon extends AbstractListModel implements Runnable {

    private final ArrayList<Abonat> abonati;

    //definirea comparatorului cu lambda expresion - sugestie NetBeans
    Comparator dupaNume = (Comparator) (Object o1, Object o2) -> {
        Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
        return a1.getNume().compareTo(a2.getNume());
    };
    /*
     //definirea comparatorului fara lambda expresion
     Comparator dupaNume = new Comparator() {
     @Override
     public int compare(Object o1, Object o2) {
     Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
     return a1.getNume().compareTo(a2.getNume());
     }
     };
     */
    Comparator dupaPrenume = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
            return a1.getPrenume().compareTo(a2.getPrenume());
        }
    };

    Comparator dupaCNP = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
            return a1.getCNP().compareTo(a2.getCNP());
        }
    };

    Comparator dupaNrTel = new Comparator() {

        @Override
        public int compare(Object o1, Object o2) {
            Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
            return a1.getNrTel().getNrTel().compareTo(a2.getNrTel().getNrTel());

        }

    };

    public CarteDeTelefon(Abonat abonatul) {

        this.abonati = new ArrayList<>();
        abonati.add(abonatul);

    }

    /**
     *
     * caut intai daca il am in lista daca il am ies din rutina fara sa il
     * duplic altfel il adaug in lista
     *
     */
    public void adaugare(Abonat abonatul) {

        for (int i = 0; i < this.getSize(); i++) {
            if (this.getElementAt(i).equals(abonatul)) {
                return;
            }
        }
        abonati.add(abonatul);
        //actualizare modificari in jList
        fireContentsChanged(this, 0, getSize());
    }

    public void stergere(Abonat abonatul) {
        abonati.remove(abonatul);
        //actualizare modificari in jList
        fireContentsChanged(this, 0, getSize());
    }

    public void stergere(int pozitie) {
        abonati.remove(pozitie);
        //actualizare modificari in jList
        fireContentsChanged(this, 0, getSize());
    }

    public void modificare(int index, Abonat abonatul) {
        abonati.set(index, abonatul);
        //actualizare modificari in jList
        fireContentsChanged(this, 0, getSize());
    }

    @Override
    public String toString() {
        return abonati.toString();
    }

    public void sortare(Comparator comparator) {
        Collections.sort(abonati, comparator);
        //actualizare modificari in jList
        fireContentsChanged(this, -1, -1);
    }

    /**
     * Metoda primeste un sir de cautat
     *
     * @param sir - un sir de cautat
     * @return -1 daca nu a gasit sirul de cautat sau un sir de indecsi de
     * abonat in campurile carora s-a gasit sirul cautat
     */
    public String search(String sir) {
        Abonat ab = (new Abonat("Test", "Search", "1234567890", "0123230432"));
        StringBuilder index = new StringBuilder("");
        for (int i = 0; i < this.getSize(); i++) {
            ab = (Abonat) this.getElementAt(i);
            if (ab.getNume().contains(sir) || ab.getPrenume().contains(sir)
                    || ab.getCNP().contains(sir) || ab.getNrTel().getNrTel().contains(sir)) {
                index.append(i);
            }
        }

        if (index.length() == 0) {
            index.append("-1");
        }
        return index.toString();
    }

    /**
     *
     * @param sir - un sir de cautat
     * @param rezultatCuAbonati Lista cu abonati de tipul CarteDeTelefon
     * @return -1 daca nu a gasit sirul de cautat sau
     * @return un sir de indecsi de abonat in campurile carora s-a gasit sirul
     * cautat
     */
    public String search(String sir, CarteDeTelefon rezultatCuAbonati) {

        Abonat ab = (new Abonat("Test", "Search", "1234567890", "0123230432"));
        StringBuilder index = new StringBuilder("");
        int j = 0;
        //rezultatCuAbonati.stergereCompleta();
        for (int i = 0; i < this.getSize(); i++) {
            ab = (Abonat) this.getElementAt(i);

            if (ab.getNume().contains(sir) || ab.getPrenume().contains(sir)
                    || ab.getCNP().contains(sir)
                    || ab.getNrTel().getNrTel().contains(sir)) {
                index.append(i);
                rezultatCuAbonati.adaugare(ab);
                j++;
            }
        }
        if (index.length() == 0) {
            index.append("-1");
        }
        return index.toString();
    }

    /**
     * Sterge abonatii unul cate unul dintr-o CarteDeTelefon
     */
    public void stergereCompleta() {

        if (this.getSize() != 0) {
            for (int i = this.getSize() - 1; i < 0; i--) {
                Abonat remove = abonati.remove(i);
            }
        }
        fireContentsChanged(this, 0, getSize());
    }

    @Override
    public int getSize() {
        return abonati.size();
    }

    @Override
    public Object getElementAt(int index) {
        return abonati.get(index);
    }

    /**
     * salveaza CarteaDeTelefon in fisierul agenda.tel
     *
     */
    public synchronized void salveaza() {

        File file = new File("agenda.tel");
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            for (Abonat ab : this.abonati) {
                oos.writeObject(ab);
            }
            oos.close();
            fos.close();
            System.out.println("Finalizare salvare in fisier.");

        } catch (IOException ex) {
            Logger.getLogger(CarteDeTelefon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * incarca in obiectul CarteaDeTelefon din fisierul primit ca argument 
     *
     * @param fisier
     * @return CarteaDeTelefon din fisier
     *
     */
    public synchronized CarteDeTelefon incarca(File file) {

        CarteDeTelefon carteIncarcata = new CarteDeTelefon(new Abonat(" ", " ", " ", " "));
        carteIncarcata.stergere(0);
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            try {
                while (fis.available() > 0) {
                    carteIncarcata.adaugare((Abonat) ois.readObject());
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CarteDeTelefon.class.getName()).log(Level.SEVERE, null, ex);
            }
            ois.close();
            fis.close();

        } catch (IOException ex) {
            Logger.getLogger(CarteDeTelefon.class.getName()).log(Level.SEVERE, null, ex);
        }

        return carteIncarcata;

    }
/**
 * cauta daca mai exista in CarteDeTelefon(this) alt abonat cu acest CNP
 * fct trebuie apelata cu un ob CarteDeTelefon
 * Returneaza true daca la cautare primeste alt sir decat "-1"(negasit)
 * @param abDeTest
 * @return 
 */
    public boolean isAbonatDuplicat(Abonat abDeTest) {
        return (!"-1".equals(this.search(abDeTest.getCNP())));
    }

    @Override
    public void run() {
        //Thread salvare automata
        int i = 1;
        while (true) {
            this.salveaza();
            System.out.println(i++ + " " + this.toString());
            try {
                Thread.sleep(60000);
            } catch (InterruptedException ex) {
                Logger.getLogger(CarteDeTelefon.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
