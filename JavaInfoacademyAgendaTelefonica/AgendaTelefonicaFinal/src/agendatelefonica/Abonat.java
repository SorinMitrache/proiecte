/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatelefonica;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author sorin
 */
public class Abonat implements Serializable {

    /**
     *
     */
    public int LATIME_COLOANA = 20;
    private String nume;
    private String prenume;
    private String CNP;
    private NrTel nrTel;

    public void setNrTel(String nrTelNou) {
        this.nrTel = new NrTel(nrTelNou);
    }

    public Abonat(String nume, String prenume, String CNP, String nrTel) {
        this.nume = nume;
        this.prenume = prenume;
        this.CNP = CNP;
        this.nrTel = new NrTel(nrTel);
    }

    @Override
    public boolean equals(Object obj) {

        final Abonat other = (Abonat) obj;
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        if (!Objects.equals(this.nume, other.nume)) {
            return false;
        }
        if (!Objects.equals(this.prenume, other.prenume)) {
            return false;
        }
        if (!Objects.equals(this.CNP, other.CNP)) {
            return false;
        }
        return this.nrTel.equals(other.nrTel);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.nume);
        hash = 41 * hash + Objects.hashCode(this.prenume);
        hash = 41 * hash + Objects.hashCode(this.CNP);
        hash = 41 * hash + Objects.hashCode(this.nrTel);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sir = new StringBuilder("");
        sir.append(this.nume);
        for (int i = 0; i < LATIME_COLOANA - this.nume.length(); i++) {
            sir.append(" ");
        }
        sir.append(this.prenume);
        for (int i = 0; i < LATIME_COLOANA - this.prenume.length(); i++) {
            sir.append(" ");
        }
        sir.append(this.CNP);
        for (int i = 0; i < LATIME_COLOANA - this.CNP.length(); i++) {
            sir.append(" ");
        }
        sir.append(this.nrTel.toString());

        return sir.toString();

    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getCNP() {
        return CNP;
    }

    public NrTel getNrTel() {
        return nrTel;
    }

    public String getNrTelString() {
        return nrTel.getNrTel();
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }
/**
 * Metoda isAbonatValid() utilizeaza REGEX pentru a verifica forma unui abonat
 * @return true daca e valid ca structura
 */
    public boolean isAbonatValid() {
        boolean test;
        test = this.getNume().matches("[A-Z]{1}+[a-z- ]{2,19}");
        test = test && this.getPrenume().matches("[A-Z]{1}+[a-z- ]{2,19}");
        test = test && this.getCNP().matches("\\d{13}");
        test = test && this.getNrTelString().matches("[0]+\\d{9}");
        return test;
    }

    /**
     * Metoda main pentru testarea obiectelor Abonat
     * @param args 
     */
    
    public static void main(String args[]) {
        ArrayList<Abonat> listaAb = new ArrayList<>();
        listaAb.add(new Abonat("Ion", "Popescu", "123456789", "0723230431"));
        listaAb.add(new Abonat("Mihai", "Ionescu", "1234567890", "0123230432"));
        System.out.println(listaAb.get(0).toString());
        System.out.println(listaAb.get(1).toString());
    }

}
