/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatelefonica;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author sorin
 */
public class JLabelReclame extends javax.swing.JLabel implements Runnable {

    private List<File> listaFisierePoza = new ArrayList<>();
    //voi transforma ArrayListul de fisiere in ArrayList de Iconuri ca sa reduc 
    //accesul inutil la sistemul de fisiere
    private final List<ImageIcon> listaIconuriReclama = new ArrayList<>();
    private int indexPozaCurenta = 0;
    private String currentDirectoryPathString;

    @Override
    public void run() {
        this.setHorizontalAlignment(CENTER);
        File file = new File(".");
        File dirCurent;
        currentDirectoryPathString = file.getAbsolutePath();
        currentDirectoryPathString = currentDirectoryPathString.substring(0, 
                (currentDirectoryPathString.length() - 1)) + "src"
                +File.separator+"agendatelefonica"+File.separator
                +"PozeReclama"+File.separator;
        dirCurent = new File(currentDirectoryPathString);
        FileFilter filtruJPG = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().toLowerCase().endsWith(".jpg");
            }
        };
        File[] listaFisiere = dirCurent.listFiles(filtruJPG);
        listaFisierePoza = Arrays.asList(listaFisiere);
        for (int j = 0; j < listaFisierePoza.size(); j++) {
            listaIconuriReclama.add(j, new ImageIcon(listaFisierePoza.get(j).getAbsolutePath()));
        }
        while (true) {
            indexPozaCurenta = ++indexPozaCurenta % listaFisierePoza.size();
            this.setIcon(listaIconuriReclama.get(indexPozaCurenta));
            this.repaint();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(JLabelReclame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
