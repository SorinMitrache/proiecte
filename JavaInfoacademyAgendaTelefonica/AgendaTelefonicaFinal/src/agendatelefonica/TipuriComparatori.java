/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendatelefonica;

import java.util.Comparator;

/**
 *
 * @author sorin
 */
//Nefolosita pt ca am mutat membrii in Clasa CarteDeTelefon 
public class TipuriComparatori implements Comparator<Abonat> {
    
     //definirea comparatorului cu lambda expresion - sugestie NetBeans
    Comparator dupaNume = (Comparator) (Object o1, Object o2) -> {
        Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
        return a1.getNume().compareTo(a2.getNume());
    };
    /*
     //definirea comparatorului fara lambda expresion
     Comparator dupaNume = new Comparator() {
     @Override
     public int compare(Object o1, Object o2) {
     Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
     return a1.getNume().compareTo(a2.getNume());
     }
     };
     */
    Comparator dupaPrenume = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
            return a1.getPrenume().compareTo(a2.getPrenume());
        }
    };

    Comparator dupaCNP = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Abonat a1 = (Abonat) o1, a2 = (Abonat) o2;
            return a1.getCNP().compareTo(a2.getCNP());
        }
    };

    @Override
    public int compare(Abonat o1, Abonat o2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
