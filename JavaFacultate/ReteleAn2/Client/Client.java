/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author sorin
 */
public class Client {

    public static void main(String[] sir) {//throws IOException {
        DateClient raport = null;
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        try {
            Socket cs = new Socket("192.168.109.1", 7777);
            System.out.println(cs.toString());
            ois = new ObjectInputStream(cs.getInputStream());
            oos = new ObjectOutputStream(cs.getOutputStream());
        } catch (Exception e) {
            System.out.println("Conectare esuata:" + e.toString());
            System.exit(0);
        }
        while (true) {
            try {
                try {
                    raport = (DateClient) ois.readObject();
                    //System.out.println("Primit obiect:"+raport.toString());
                } catch (ClassNotFoundException e) {
                    System.out.println("S-a pierdut legatura cu serverul");
                    System.exit(0);
                    //e.printStackTrace();
                }

                raport.resetObiect();
                oos.writeObject(raport);

            } catch (IOException ex) {
                System.out.println("S-a pierdut legatura cu serverul");
                System.exit(0);
                //Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
