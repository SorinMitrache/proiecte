package Server;

/**
 * Created by sorin on 29.05.2016.
 */
// Unitatea de compilare Up_S
import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class Server {

    public static void main(String[] arg) throws IOException {
        //initializare fisier pt salvarea datelor
        File file = new File("IstoricInterogari.csv");
        FileWriter fw = null;
        try {
            fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            out.println("Nume_Statie, Ora, Minut, Secunda");
            out.close();
            bw.close();
            fw.close();

        } catch (IOException ex) {
            System.out.println("Eroare la deschiderea fisierului.");
            System.exit(0);
            //Logger.getLogger(DateDeSalvat.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Fisierul pentru salvari a fost initializat.");
        //initializare server
        ServerSocket ss = new ServerSocket(7777);
        System.out.println("Serverul a pornit.");
        Socket cs = null;
        //asteptare clienti
        while (true) {
            cs = ss.accept();
            System.out.print("Client nou: ");
            new ConexiuneClient(cs);
        }
    }
}
