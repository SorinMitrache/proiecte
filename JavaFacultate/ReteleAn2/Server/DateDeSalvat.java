package Server;

import Client.*;
//import Client.DateClient;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sorin on 29.05.2016.
 */
public class DateDeSalvat implements Serializable {
//tb schimbate atr. predefinite cu Stringuri

    //protected InetAddress nume;
    //protected Date data;
    protected String numeClient;
    protected int ora;
    protected int minut;
    protected int secunda;//hh:mm:ss

    public DateDeSalvat() {
        numeClient = "Initial";
        ora = 0;
        minut = 0;
        secunda = 0;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public int getOra() {
        return ora;
    }

    public void setOra(int ora) {
        this.ora = ora;
    }

    public int getMinut() {
        return minut;
    }

    public void setMinut(int minut) {
        this.minut = minut;
    }

    public int getSecunda() {
        return secunda;
    }

    public void setSecunda(int secunda) {
        this.secunda = secunda;
    }

    public DateDeSalvat resetObiect() {
        try {
            String sNume = InetAddress.getLocalHost().toString();
            this.numeClient = sNume.substring(0, sNume.indexOf("/"));
        } catch (UnknownHostException e) {
            System.out.println("Gazda nu are adresa IP");
        }
        Calendar cal = Calendar.getInstance();
        this.ora = cal.get(Calendar.HOUR_OF_DAY);
        this.minut = cal.get(Calendar.MINUTE);
        this.secunda = cal.get(Calendar.SECOND);
        return this;
    }

    public DateDeSalvat importa(DateClient dc) {
        this.setNumeClient(dc.getNumeClient());
        this.setOra(dc.getOra());
        this.setMinut(dc.getMinut());
        this.setSecunda(dc.getSecunda());
        return this;
    }

    public synchronized DateDeSalvat salveaza() {
        File file = new File("IstoricInterogari.csv");
        FileWriter fw = null;
        try {
            fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            out.println(this.toStringCSV());
            out.close();
            bw.close();
            fw.close();

        } catch (IOException ex) {
            Logger.getLogger(DateDeSalvat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String toString() {
        return "Client{"
                + "numeClient='" + numeClient + '\''
                + ", ora=" + ora
                + ", minut=" + minut
                + ", secunda=" + secunda
                + '}';
    }

    public String toStringCSV() {
        return numeClient + "," + ora + "," + minut + "," + secunda;
    }
}
