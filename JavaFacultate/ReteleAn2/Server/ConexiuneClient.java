/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import Client.DateClient;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

class ConexiuneClient extends Thread {

    Socket cs = null;
    ObjectInputStream ois = null;
    ObjectOutputStream oos = null;

    public ConexiuneClient(Socket client) {//throws IOException {

        try {
            
            cs = client;
            System.out.println(cs.toString());
            oos = new ObjectOutputStream(cs.getOutputStream());
            InputStream is = cs.getInputStream();
            ois = new ObjectInputStream(is);
            start();
        } catch (IOException ex) {
            System.out.println("S-a pierdut legatura catre client " );

            System.exit(0);
            //Logger.getLogger(ConexiuneClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        DateClient raport = new DateClient();
        DateDeSalvat dateDeSalvat;
        System.out.println(raport.toString());
        while (true) {
            try {
                oos.writeObject(raport);
                System.out.println("Trimis comanda.");
                try {
                    raport = (DateClient) ois.readObject();
                } catch (ClassNotFoundException e) {
                }
                Toolkit.getDefaultToolkit().beep();
                System.out.println("Am primit datele: " + raport.toString());
                //Calculeaza diferenta de minute intre client si server
                int diferentaMinute = Math.abs((new DateClient()).resetObiect().getMinut() - raport.getMinut()
                        + (new DateClient()).resetObiect().getOra() * 60 - raport.getOra() * 60);
                //System.out.println((new DateClient()).resetObiect().getMinut());

                if (diferentaMinute > 5) {
                    dateDeSalvat = (new DateDeSalvat()).importa(raport);
                    dateDeSalvat.salveaza();
                    for (int i = 0; i < 5; i++) {
                        Toolkit.getDefaultToolkit().beep();
                        sleep(150);
                    }
                }
                sleep(20000);
            } catch (IOException e) {
                System.out.println(e.toString());
                //daca nu ii raspunde clientul inchide conexiunea si streamurile
                try {
                    oos.close();
                    ois.close();
                    cs.close();
                } catch (IOException e2) {
                    System.out.println("Client deconectat: " + raport.toString());
//e2.printStackTrace();
                }
            } catch (InterruptedException e) {
                System.out.println("Client deconectat: " + raport.toString());
                //e.printStackTrace();

            }
        }

    }
}
