package QuickSort;

public class QuickSort {
    private static int partitionare (int n[], int stanga, int dreapta) {
        int i = stanga, j = dreapta;
        int pivot = n[((stanga + dreapta) / 2)];
        while (i <= j) {
            while (n[i] < pivot) {
                i++;
            }
            while (n[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int tmp = n[i];
                n[i]= n[j];
                n[j] = tmp;
                i++;
                j--;
            }
        }
        return i;
    }
    public static void quickSort(int n[], int stanga, int dreapta) {
        int index = partitionare(n, stanga, dreapta);
        if (stanga < index - 1) {
            quickSort(n,  stanga, index - 1);
        }
        if (index < dreapta) {
            quickSort(n, index, dreapta);
        }
    }
}
