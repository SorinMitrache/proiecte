package sumainbancnote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author sorin
 */
public class SumaInBancnote {

    final int bancnote[] = new int[]{500, 100, 50, 10, 5, 1};
    final int cantitati[] = new int[]{3, 4, 10, 10, 100, 1000};
    private int bucati[] = new int[bancnote.length];
    int suma, maxim = 0, nrBancnote = 0;

    //constructor fara argumente
    public SumaInBancnote() {
        for (int i = 0; i < bancnote.length; i++) {
            bucati[i]=0; 
        }
    }
    //setteri
    public void setBucati(int[] bucati) {
        this.bucati = bucati;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }

    public void setMaxim(int maxim) {
        this.maxim = maxim;
    }

    public void setNrBancnote(int nrBancnote) {
        this.nrBancnote = nrBancnote;
    }


    public int[] getBancnote() {
        return bancnote;
    }

    public int[] getCantitati() {
        return cantitati;
    }

    public int[] getBucati() {
        return bucati;
    }

    public int getSuma() {
        return suma;
    }

    public int getMaxim() {
        return maxim;
    }

    public int getNrBancnote() {
        return nrBancnote;
    }

    public void greedy() {
        for (int i = 0; i < bancnote.length; i++) {
            if (suma >= bancnote[i]) {//testez daca pot da aceasta bancnota de ordin i
                if (suma / bancnote[i] < cantitati[i]) {
                    bucati[i] = suma / bancnote[i];//se ia un numar intreg de bancnote
                    suma %= bancnote[i];//rest de dat in continuare
                } else {
                    bucati[i] = cantitati[i];//se iau toate bancnotele disponibile
                    suma -= bancnote[i] * cantitati[i];//rest de dat in continuare
                }
            } 
            if (suma == 0) {
                return;
            }
        }
    }


    public static void main(String[] args) {
        MainFrame mf = new MainFrame();
    }

}
