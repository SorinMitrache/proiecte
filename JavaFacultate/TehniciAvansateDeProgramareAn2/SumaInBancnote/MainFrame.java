package sumainbancnote;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author sorin
 */
public class MainFrame extends JFrame {

    private SumaInBancnote sib;
    private JButton b;
    private JTextArea ta;
    private JTextField tf;
    private JFrame f;
    private JPanel p;
    private boolean readyToExit;

    private void initComponents() {
        //tb schimbat astfel incat sa primeasaca date printr-un inputDialog testa cu s!=null si nu prin tf
        //creare componente grafice
        f = new JFrame();
        p = new JPanel();
        ta = new JTextArea();
        tf = new JTextField();
        b = new JButton();

        //initializare componente grafice
        f.setName("Calculul numarului minim de bancnote");
        ta.setName("Afisare Date");
        tf.setText("    ");
        b.setText("Apasati pentru a calcula.");
        //setare layout panel
        p.setLayout(new BorderLayout());
        //adaugare componente in panel
        p.add(ta, "Center");
        p.add(tf, "North");
        p.add(b, "South");

        f.add(p);// adaugare panel in frame
        f.setSize(800, 600);
        f.setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);

        b.addActionListener(new ActionListener() {
            @Override //clasa interioara anonima
            public void actionPerformed(ActionEvent evt) {
                bActionPerformed(evt);
            }
        });

    }

    public MainFrame() {
        readyToExit = false;
        initComponents();
        sib = new SumaInBancnote();
        ta.setText("Monetar:");
        for (int i = 0; i < sib.getBancnote().length; i++) {
            ta.append("\n" + sib.getCantitati()[i] + " bancnote de " + (sib.getBancnote()[i]) + " = " + sib.getCantitati()[i] * sib.getBancnote()[i]);
            sib.setMaxim(sib.getMaxim() + sib.getCantitati()[i] * sib.getBancnote()[i]);//maxim += cantitati[i] * bancnote[i];
            sib.setNrBancnote(sib.getNrBancnote() + sib.getCantitati()[i]);//nrBancnote += cantitati[i];
        }
        ta.append("\n\nSuma maxima disponibila in casa este " + sib.getMaxim() + " lei in " + sib.getNrBancnote() + " bancnote.\n");

        boolean ok = true;
        while (ok) {
            try {
                // Posibila aparitie a exceptiei de nr cu zecimala sau caractere nonnumerice:
                String s = JOptionPane.showInputDialog(f, "Introduceti un numar intreg pozitiv mai mic decat " + sib.getMaxim(),
                        "Introducere valoare.", JOptionPane.INFORMATION_MESSAGE);
                if (s != null) {
                    int val = Integer.parseInt(s);
                    //Posibil aparitie a erorii de numar negativ sau mai mare decat casa (de bani)
                    if (val <= 0 || val > sib.getMaxim()) {
                        JOptionPane.showMessageDialog(null, "Introduceti un numar intreg pozitiv mai mic decat " + sib.getMaxim(),
                                "Eroare de introducere", JOptionPane.ERROR_MESSAGE);
                    } else {
                        sib.setSuma(val);
                        ok = false;
                    }
                }

            } catch (NumberFormatException err) {
                //Cod pt tratarea tuturor erorilor de format
                JOptionPane.showMessageDialog(null, "Introduceti un numar intreg.",
                        "Eroare de introducere", JOptionPane.ERROR_MESSAGE);
                System.out.println(err);
            }

        }
        tf.setText("Suma de restituit este " + sib.getSuma() + " lei.");

    }

    private void bActionPerformed(ActionEvent evt) {
        if (readyToExit) {
            System.exit(0);
        } else {
            sib.greedy();
            sib.setNrBancnote(0);

            for (int i = 0; i < sib.getBancnote().length; i++) {
                if (sib.getBucati()[i] != 0) {
                    sib.setNrBancnote(sib.getNrBancnote() + sib.getBucati()[i]);//nrBancnote += bucati[i];
                    ta.append("\n" + sib.getBucati()[i] + " bancnote din " + sib.getCantitati()[i] + " de " + sib.getBancnote()[i] + " = " + sib.getBucati()[i] * sib.getBancnote()[i]);
                }
            }
            ta.append("\n\nNumarul minim de bancnote este " + sib.getNrBancnote());
            b.setText("Exit");
            readyToExit = true;

        }
    }
    
}
